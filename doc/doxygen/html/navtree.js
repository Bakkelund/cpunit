var NAVTREE =
[
  [ "CPUnit", "index.html", [
    [ "Class List", "annotated.html", [
      [ "cpunit::AnyType", "structcpunit_1_1_any_type.html", null ],
      [ "cpunit::AssertionException", "classcpunit_1_1_assertion_exception.html", null ],
      [ "cpunit::BasicTestRunner", "classcpunit_1_1_basic_test_runner.html", null ],
      [ "cpunit::impl::BootStream", "classcpunit_1_1impl_1_1_boot_stream.html", null ],
      [ "cpunit::Callable", "classcpunit_1_1_callable.html", null ],
      [ "cpunit::CmdLineParser", "classcpunit_1_1_cmd_line_parser.html", null ],
      [ "cpunit::ConformityChecker", "classcpunit_1_1_conformity_checker.html", null ],
      [ "cpunit::CPUnitException", "classcpunit_1_1_c_p_unit_exception.html", null ],
      [ "cpunit::ErrorReportFormat", "classcpunit_1_1_error_report_format.html", null ],
      [ "cpunit::ExceptionExpectedCall< ExceptionType >", "classcpunit_1_1_exception_expected_call.html", null ],
      [ "cpunit::ExceptionExpectedCall< AnyType >", "classcpunit_1_1_exception_expected_call_3_01_any_type_01_4.html", null ],
      [ "cpunit::ExceptionTestRegistrar< ExceptionType >", "classcpunit_1_1_exception_test_registrar.html", null ],
      [ "cpunit::ExecutionReport", "classcpunit_1_1_execution_report.html", null ],
      [ "cpunit::FixtureRegistrar", "classcpunit_1_1_fixture_registrar.html", null ],
      [ "cpunit::FuncTestRegistrar", "classcpunit_1_1_func_test_registrar.html", null ],
      [ "cpunit::FunctionCall", "classcpunit_1_1_function_call.html", null ],
      [ "cpunit::GlobMatcher", "classcpunit_1_1_glob_matcher.html", null ],
      [ "cpunit::IllegalArgumentException", "classcpunit_1_1_illegal_argument_exception.html", null ],
      [ "cpunit::RegInfo", "classcpunit_1_1_reg_info.html", null ],
      [ "cpunit::RunAllTestRunner", "classcpunit_1_1_run_all_test_runner.html", null ],
      [ "cpunit::impl::SafeStream", "classcpunit_1_1impl_1_1_safe_stream.html", null ],
      [ "cpunit::SafeTearDown", "classcpunit_1_1_safe_tear_down.html", null ],
      [ "cpunit::StopWatch", "classcpunit_1_1_stop_watch.html", null ],
      [ "cpunit::StringFlyweightStore::str_ptr_cmp", "structcpunit_1_1_string_flyweight_store_1_1str__ptr__cmp.html", null ],
      [ "cpunit::impl::StrCat", "classcpunit_1_1impl_1_1_str_cat.html", null ],
      [ "cpunit::StringFlyweightStore", "classcpunit_1_1_string_flyweight_store.html", null ],
      [ "cpunit::StringFlyweightStoreUsage", "classcpunit_1_1_string_flyweight_store_usage.html", null ],
      [ "cpunit::TestExecutionFacade", "classcpunit_1_1_test_execution_facade.html", null ],
      [ "cpunit::TestRunner", "classcpunit_1_1_test_runner.html", null ],
      [ "cpunit::TestRunnerDecorator", "classcpunit_1_1_test_runner_decorator.html", null ],
      [ "cpunit::TestStore", "classcpunit_1_1_test_store.html", null ],
      [ "cpunit::TestTreeNode", "classcpunit_1_1_test_tree_node.html", null ],
      [ "cpunit::TestUnit", "classcpunit_1_1_test_unit.html", null ],
      [ "cpunit::TimeFormat", "classcpunit_1_1_time_format.html", null ],
      [ "cpunit::TimeGuardRunner", "classcpunit_1_1_time_guard_runner.html", null ],
      [ "cpunit::WrongSetupException", "classcpunit_1_1_wrong_setup_exception.html", null ]
    ] ],
    [ "Class Index", "classes.html", null ],
    [ "Class Hierarchy", "hierarchy.html", [
      [ "cpunit::AnyType", "structcpunit_1_1_any_type.html", null ],
      [ "cpunit::impl::BootStream", "classcpunit_1_1impl_1_1_boot_stream.html", null ],
      [ "cpunit::Callable", "classcpunit_1_1_callable.html", [
        [ "cpunit::ExceptionExpectedCall< ExceptionType >", "classcpunit_1_1_exception_expected_call.html", null ],
        [ "cpunit::ExceptionExpectedCall< AnyType >", "classcpunit_1_1_exception_expected_call_3_01_any_type_01_4.html", null ],
        [ "cpunit::FunctionCall", "classcpunit_1_1_function_call.html", null ]
      ] ],
      [ "cpunit::CmdLineParser", "classcpunit_1_1_cmd_line_parser.html", null ],
      [ "cpunit::ConformityChecker", "classcpunit_1_1_conformity_checker.html", null ],
      [ "cpunit::CPUnitException", "classcpunit_1_1_c_p_unit_exception.html", [
        [ "cpunit::AssertionException", "classcpunit_1_1_assertion_exception.html", null ],
        [ "cpunit::IllegalArgumentException", "classcpunit_1_1_illegal_argument_exception.html", null ],
        [ "cpunit::WrongSetupException", "classcpunit_1_1_wrong_setup_exception.html", null ]
      ] ],
      [ "cpunit::ErrorReportFormat", "classcpunit_1_1_error_report_format.html", null ],
      [ "cpunit::ExceptionTestRegistrar< ExceptionType >", "classcpunit_1_1_exception_test_registrar.html", null ],
      [ "cpunit::ExecutionReport", "classcpunit_1_1_execution_report.html", null ],
      [ "cpunit::FixtureRegistrar", "classcpunit_1_1_fixture_registrar.html", null ],
      [ "cpunit::FuncTestRegistrar", "classcpunit_1_1_func_test_registrar.html", null ],
      [ "cpunit::GlobMatcher", "classcpunit_1_1_glob_matcher.html", null ],
      [ "cpunit::RegInfo", "classcpunit_1_1_reg_info.html", null ],
      [ "cpunit::impl::SafeStream", "classcpunit_1_1impl_1_1_safe_stream.html", null ],
      [ "cpunit::SafeTearDown", "classcpunit_1_1_safe_tear_down.html", null ],
      [ "cpunit::StopWatch", "classcpunit_1_1_stop_watch.html", null ],
      [ "cpunit::StringFlyweightStore::str_ptr_cmp", "structcpunit_1_1_string_flyweight_store_1_1str__ptr__cmp.html", null ],
      [ "cpunit::impl::StrCat", "classcpunit_1_1impl_1_1_str_cat.html", null ],
      [ "cpunit::StringFlyweightStore", "classcpunit_1_1_string_flyweight_store.html", null ],
      [ "cpunit::StringFlyweightStoreUsage", "classcpunit_1_1_string_flyweight_store_usage.html", null ],
      [ "cpunit::TestExecutionFacade", "classcpunit_1_1_test_execution_facade.html", null ],
      [ "cpunit::TestRunner", "classcpunit_1_1_test_runner.html", [
        [ "cpunit::BasicTestRunner", "classcpunit_1_1_basic_test_runner.html", null ],
        [ "cpunit::TestRunnerDecorator", "classcpunit_1_1_test_runner_decorator.html", [
          [ "cpunit::RunAllTestRunner", "classcpunit_1_1_run_all_test_runner.html", null ],
          [ "cpunit::TimeGuardRunner", "classcpunit_1_1_time_guard_runner.html", null ]
        ] ]
      ] ],
      [ "cpunit::TestStore", "classcpunit_1_1_test_store.html", null ],
      [ "cpunit::TestTreeNode", "classcpunit_1_1_test_tree_node.html", null ],
      [ "cpunit::TestUnit", "classcpunit_1_1_test_unit.html", null ],
      [ "cpunit::TimeFormat", "classcpunit_1_1_time_format.html", null ]
    ] ],
    [ "Class Members", "functions.html", null ],
    [ "Namespace List", "namespaces.html", [
      [ "cpunit", "namespacecpunit.html", null ],
      [ "cpunit::impl", "namespacecpunit_1_1impl.html", null ],
      [ "cpunit::priv", "namespacecpunit_1_1priv.html", null ]
    ] ],
    [ "Namespace Members", "namespacemembers.html", null ],
    [ "File List", "files.html", [
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit.hpp", "cpunit_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_Assert.cpp", "cpunit___assert_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_Assert.hpp", "cpunit___assert_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_Assert.tcc", "cpunit___assert_8tcc.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_AssertionException.cpp", "cpunit___assertion_exception_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_AssertionException.hpp", "cpunit___assertion_exception_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_BasicTestRunner.cpp", "cpunit___basic_test_runner_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_BasicTestRunner.hpp", "cpunit___basic_test_runner_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_Callable.cpp", "cpunit___callable_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_Callable.hpp", "cpunit___callable_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_CmdLineParser.cpp", "cpunit___cmd_line_parser_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_CmdLineParser.hpp", "cpunit___cmd_line_parser_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_CmdLineParser.tcc", "cpunit___cmd_line_parser_8tcc.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_ConformityChecker.cpp", "cpunit___conformity_checker_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_ConformityChecker.hpp", "cpunit___conformity_checker_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_CPUnitException.cpp", "cpunit___c_p_unit_exception_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_CPUnitException.hpp", "cpunit___c_p_unit_exception_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_EntryPoint.cpp", "cpunit___entry_point_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_EntryPoint.hpp", "cpunit___entry_point_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_ErrorReportFormat.cpp", "cpunit___error_report_format_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_ErrorReportFormat.hpp", "cpunit___error_report_format_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_ExceptionExpectedCall.cpp", "cpunit___exception_expected_call_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_ExceptionExpectedCall.hpp", "cpunit___exception_expected_call_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_ExceptionExpectedCall.tcc", "cpunit___exception_expected_call_8tcc.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_ExceptionTestRegistrar.hpp", "cpunit___exception_test_registrar_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_ExceptionTestRegistrar.tcc", "cpunit___exception_test_registrar_8tcc.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_ExecutionReport.cpp", "cpunit___execution_report_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_ExecutionReport.hpp", "cpunit___execution_report_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_FixtureRegistrar.cpp", "cpunit___fixture_registrar_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_FixtureRegistrar.hpp", "cpunit___fixture_registrar_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_FuncTestRegistrar.cpp", "cpunit___func_test_registrar_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_FuncTestRegistrar.hpp", "cpunit___func_test_registrar_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_FunctionCall.cpp", "cpunit___function_call_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_FunctionCall.hpp", "cpunit___function_call_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_GlobMatcher.cpp", "cpunit___glob_matcher_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_GlobMatcher.hpp", "cpunit___glob_matcher_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_IllegalArgumentException.cpp", "cpunit___illegal_argument_exception_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_IllegalArgumentException.hpp", "cpunit___illegal_argument_exception_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_impl_BootStream.cpp", "cpunit__impl___boot_stream_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_impl_BootStream.hpp", "cpunit__impl___boot_stream_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_impl_StrCat.cpp", "cpunit__impl___str_cat_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_impl_StrCat.hpp", "cpunit__impl___str_cat_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_impl_StrCat.tcc", "cpunit__impl___str_cat_8tcc.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_Main.cpp", "cpunit___main_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_Ostreams.hpp", "cpunit___ostreams_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_Ostreams.tcc", "cpunit___ostreams_8tcc.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_RegInfo.cpp", "cpunit___reg_info_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_RegInfo.hpp", "cpunit___reg_info_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_RunAllTestRunner.cpp", "cpunit___run_all_test_runner_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_RunAllTestRunner.hpp", "cpunit___run_all_test_runner_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_StopWatch.cpp", "cpunit___stop_watch_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_StopWatch.hpp", "cpunit___stop_watch_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_StringFlyweightStore.cpp", "cpunit___string_flyweight_store_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_StringFlyweightStore.hpp", "cpunit___string_flyweight_store_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_TestExecutionFacade.cpp", "cpunit___test_execution_facade_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_TestExecutionFacade.hpp", "cpunit___test_execution_facade_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_TestRunner.hpp", "cpunit___test_runner_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_TestRunnerDecorator.cpp", "cpunit___test_runner_decorator_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_TestRunnerDecorator.hpp", "cpunit___test_runner_decorator_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_TestStore.cpp", "cpunit___test_store_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_TestStore.hpp", "cpunit___test_store_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_TestTreeNode.cpp", "cpunit___test_tree_node_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_TestTreeNode.hpp", "cpunit___test_tree_node_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_TestUnit.cpp", "cpunit___test_unit_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_TestUnit.hpp", "cpunit___test_unit_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_TimeFormat.cpp", "cpunit___time_format_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_TimeFormat.hpp", "cpunit___time_format_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_TimeGuardRunner.cpp", "cpunit___time_guard_runner_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_TimeGuardRunner.hpp", "cpunit___time_guard_runner_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_trace.hpp", "cpunit__trace_8hpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_WrongSetupException.cpp", "cpunit___wrong_setup_exception_8cpp.html", null ],
      [ "/Users/Shared/Development/cpp/sourceforge/cpunit_095/src/cpunit_WrongSetupException.hpp", "cpunit___wrong_setup_exception_8hpp.html", null ]
    ] ],
    [ "File Members", "globals.html", null ]
  ] ]
];

function createIndent(o,domNode,node,level)
{
  if (node.parentNode && node.parentNode.parentNode)
  {
    createIndent(o,domNode,node.parentNode,level+1);
  }
  var imgNode = document.createElement("img");
  if (level==0 && node.childrenData)
  {
    node.plus_img = imgNode;
    node.expandToggle = document.createElement("a");
    node.expandToggle.href = "javascript:void(0)";
    node.expandToggle.onclick = function() 
    {
      if (node.expanded) 
      {
        $(node.getChildrenUL()).slideUp("fast");
        if (node.isLast)
        {
          node.plus_img.src = node.relpath+"ftv2plastnode.png";
        }
        else
        {
          node.plus_img.src = node.relpath+"ftv2pnode.png";
        }
        node.expanded = false;
      } 
      else 
      {
        expandNode(o, node, false);
      }
    }
    node.expandToggle.appendChild(imgNode);
    domNode.appendChild(node.expandToggle);
  }
  else
  {
    domNode.appendChild(imgNode);
  }
  if (level==0)
  {
    if (node.isLast)
    {
      if (node.childrenData)
      {
        imgNode.src = node.relpath+"ftv2plastnode.png";
      }
      else
      {
        imgNode.src = node.relpath+"ftv2lastnode.png";
        domNode.appendChild(imgNode);
      }
    }
    else
    {
      if (node.childrenData)
      {
        imgNode.src = node.relpath+"ftv2pnode.png";
      }
      else
      {
        imgNode.src = node.relpath+"ftv2node.png";
        domNode.appendChild(imgNode);
      }
    }
  }
  else
  {
    if (node.isLast)
    {
      imgNode.src = node.relpath+"ftv2blank.png";
    }
    else
    {
      imgNode.src = node.relpath+"ftv2vertline.png";
    }
  }
  imgNode.border = "0";
}

function newNode(o, po, text, link, childrenData, lastNode)
{
  var node = new Object();
  node.children = Array();
  node.childrenData = childrenData;
  node.depth = po.depth + 1;
  node.relpath = po.relpath;
  node.isLast = lastNode;

  node.li = document.createElement("li");
  po.getChildrenUL().appendChild(node.li);
  node.parentNode = po;

  node.itemDiv = document.createElement("div");
  node.itemDiv.className = "item";

  node.labelSpan = document.createElement("span");
  node.labelSpan.className = "label";

  createIndent(o,node.itemDiv,node,0);
  node.itemDiv.appendChild(node.labelSpan);
  node.li.appendChild(node.itemDiv);

  var a = document.createElement("a");
  node.labelSpan.appendChild(a);
  node.label = document.createTextNode(text);
  a.appendChild(node.label);
  if (link) 
  {
    a.href = node.relpath+link;
  } 
  else 
  {
    if (childrenData != null) 
    {
      a.className = "nolink";
      a.href = "javascript:void(0)";
      a.onclick = node.expandToggle.onclick;
      node.expanded = false;
    }
  }

  node.childrenUL = null;
  node.getChildrenUL = function() 
  {
    if (!node.childrenUL) 
    {
      node.childrenUL = document.createElement("ul");
      node.childrenUL.className = "children_ul";
      node.childrenUL.style.display = "none";
      node.li.appendChild(node.childrenUL);
    }
    return node.childrenUL;
  };

  return node;
}

function showRoot()
{
  var headerHeight = $("#top").height();
  var footerHeight = $("#nav-path").height();
  var windowHeight = $(window).height() - headerHeight - footerHeight;
  navtree.scrollTo('#selected',0,{offset:-windowHeight/2});
}

function expandNode(o, node, imm)
{
  if (node.childrenData && !node.expanded) 
  {
    if (!node.childrenVisited) 
    {
      getNode(o, node);
    }
    if (imm)
    {
      $(node.getChildrenUL()).show();
    } 
    else 
    {
      $(node.getChildrenUL()).slideDown("fast",showRoot);
    }
    if (node.isLast)
    {
      node.plus_img.src = node.relpath+"ftv2mlastnode.png";
    }
    else
    {
      node.plus_img.src = node.relpath+"ftv2mnode.png";
    }
    node.expanded = true;
  }
}

function getNode(o, po)
{
  po.childrenVisited = true;
  var l = po.childrenData.length-1;
  for (var i in po.childrenData) 
  {
    var nodeData = po.childrenData[i];
    po.children[i] = newNode(o, po, nodeData[0], nodeData[1], nodeData[2],
        i==l);
  }
}

function findNavTreePage(url, data)
{
  var nodes = data;
  var result = null;
  for (var i in nodes) 
  {
    var d = nodes[i];
    if (d[1] == url) 
    {
      return new Array(i);
    }
    else if (d[2] != null) // array of children
    {
      result = findNavTreePage(url, d[2]);
      if (result != null) 
      {
        return (new Array(i).concat(result));
      }
    }
  }
  return null;
}

function initNavTree(toroot,relpath)
{
  var o = new Object();
  o.toroot = toroot;
  o.node = new Object();
  o.node.li = document.getElementById("nav-tree-contents");
  o.node.childrenData = NAVTREE;
  o.node.children = new Array();
  o.node.childrenUL = document.createElement("ul");
  o.node.getChildrenUL = function() { return o.node.childrenUL; };
  o.node.li.appendChild(o.node.childrenUL);
  o.node.depth = 0;
  o.node.relpath = relpath;

  getNode(o, o.node);

  o.breadcrumbs = findNavTreePage(toroot, NAVTREE);
  if (o.breadcrumbs == null)
  {
    o.breadcrumbs = findNavTreePage("index.html",NAVTREE);
  }
  if (o.breadcrumbs != null && o.breadcrumbs.length>0)
  {
    var p = o.node;
    for (var i in o.breadcrumbs) 
    {
      var j = o.breadcrumbs[i];
      p = p.children[j];
      expandNode(o,p,true);
    }
    p.itemDiv.className = p.itemDiv.className + " selected";
    p.itemDiv.id = "selected";
    $(window).load(showRoot);
  }
}

