/*
   Copyright (c) 2011 Daniel Bakkelund.
   All rights reserved.

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:
    1. Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
    3. Neither the name of the copyright holders nor the names of its
       contributors may be used to endorse or promote products derived from
       this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
   IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
   LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
   CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
   SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
   INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
   CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
   ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
   THE POSSIBILITY OF SUCH DAMAGE.
*/



#include <cpunit>
#include <cpunit_GlobMatcher.hpp>
#include <cpunit_ConformityChecker.hpp>
#include <cpunit_RegInfo.hpp>

namespace ConformityCheckerTest {

  using namespace cpunit;
  
  ConformityChecker filename_checker("*Test", "test_*", "*Test.xyz");
  ConformityChecker filepath_checker("*Test", "test_*");

  const char *test_path = "SomeTest";
  const char *test_name = "test_stuff";
  const char *test_file = "/path/SomeTest.xyz";

  CPUNIT_SET_UP(ConformityCheckerTest) {
    filename_checker.set_extension("xyz");
    filepath_checker.set_extension("xyz");

    // Validate fixture
    const GlobMatcher path("*Test");
    assert_true("path pattern corrupt.", path.matches(test_path));
    
    const GlobMatcher name("test_*");
    assert_true("name pattern corrupt", name.matches(test_name));

    const GlobMatcher file("*Test.xyz");
    assert_true("file pattern corrupt", file.matches(test_file));
  }
  
  // todo: QA tests - check the two different cases for each test.

  CPUNIT_TEST(ConformityCheckerTest, test_OK) {
    const RegInfo ri(test_path, test_name, test_file, "42");
    assert_true("Failed for filename checker", filename_checker.matches(ri));
    assert_true("Failed for file path checker", filepath_checker.matches(ri));
  }

  CPUNIT_TEST(ConformityCheckerTest, test_wrong_path) {
    const RegInfo ri("SomeTests", test_name, test_file, "42");
    assert_false("Failed for filename checker", filename_checker.matches(ri));
    assert_false("Failed for file path checker", filepath_checker.matches(ri));
  }

  CPUNIT_TEST(ConformityCheckerTest, test_wrong_name) {
    const RegInfo ri(test_path, "tst_stuff", test_file, "42");
    assert_false("Failed for filename checker", filename_checker.matches(ri));
    assert_false("Failed for file path checker", filepath_checker.matches(ri));
  }

  CPUNIT_TEST(ConformityCheckerTest, test_wrong_extension) {
    const RegInfo ri(test_path, test_name, "/path/SomeTest.cpp", "42");
    assert_false("Failed for filename checker", filename_checker.matches(ri));
    assert_false("Failed for file path checker", filepath_checker.matches(ri));
  }

  CPUNIT_TEST(ConformityCheckerTest, test_wrong_file) {
    const RegInfo ri(test_path, test_name, "/path/SomeTester.cpp", "42");
    assert_false("Failed for filename checker", filename_checker.matches(ri));
    assert_false("Failed for file path checker", filepath_checker.matches(ri));
  }

  CPUNIT_TEST(ConformityCheckerTest, test_wrong_file_path_correlation) {
    const RegInfo ri(test_path, test_name, "/path/SomeonesTest.xyz", "42");
    assert_true("Failed for filename checker", filename_checker.matches(ri));
    assert_false("Failed for file path checker", filepath_checker.matches(ri));
  }

  CPUNIT_TEST(ConformityCheckerTest, test_nested_path_OK) {
    const RegInfo ri("MyScope::InnerTest", test_name, "/path/MyScope_InnerTest.xyz", "42");
    assert_true("Failed for filename checker", filename_checker.matches(ri));
    assert_true("Failed for file path checker", filepath_checker.matches(ri));
  }

  CPUNIT_TEST(ConformityCheckerTest, test_nested_path_not_ok) {
    const RegInfo ri("MyScope::InnerTest", test_name, "/path/TheTest.xyz", "42");
    assert_true("Failed for filename checker", filename_checker.matches(ri));
    assert_false("Failed for file path checker", filepath_checker.matches(ri));
  }
}
